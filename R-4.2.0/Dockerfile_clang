FROM docker.io/redhat/ubi8:latest

ENV R_VERSION 4.2.0
ENV CC clang
ENV CXX clang++
ENV R_PLATFORM arm64_linux_rhel_ubi8

LABEL org.opencontainers.image.licenses="MIT" \
    org.opencontainers.image.vendor="Isaac Florence" \
    org.opencontainers.image.authors="Isaac Florence <isaac.florence@outlook.com>" \
    org.opencontainers.image.source="https://gitlab.com/isaac-florence/r-devbox" \
    R_VERSION="${R_VERSION}" \
    R_build="ver-${R_VERSION}_CC-clang_CXX-clang++"


## RHEL UBI8 dependencies
RUN yum install -y \
        make \
        clang \
        perl-devel \
        gcc-gfortran \
        pcre2-devel \
        zlib-devel \
        bzip2-devel \
        xz-devel \
        curl-devel 

## Get R-version, untar, cd
RUN curl -O https://cran.r-project.org/src/base/R-4/R-${R_VERSION}.tar.gz ; \
    tar -xzvf R-${R_VERSION}.tar.gz 
WORKDIR "/R-${R_VERSION}"

## Configure, build, install
RUN set -ex; \
    ./configure \
        --prefix=/opt/R/${R_VERSION} \
        --enable-memory-profiling \
        --enable-R-shlib \
        --with-blas \
        --with-lapack \
        --with-readline=no \
        --with-x=no \
    ; \
    make; \
    make install; \
    ln -s /opt/R/${R_VERSION}/bin/R /usr/local/bin/R; \
    ln -s /opt/R/${R_VERSION}/bin/Rscript /usr/local/bin/Rscript;


## Add user to 'staff' group, granting them write privileges to /usr/local/lib/R/site.library
RUN useradd docker \
    && mkdir /project \
    && chown docker:docker /project \
    && chown -R docker:docker /opt/R/4.2.0/lib/R
WORKDIR "/project"


## Tidy up
RUN rm -rf \
        /R-${R_VERSION} \
        /R-${R_VERSION}.tar.gz

## final steps
USER docker
ENTRYPOINT [ "R" ]
